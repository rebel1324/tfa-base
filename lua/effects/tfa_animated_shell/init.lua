local bvec = Vector(0, 0, 0)
local uAng = Angle(90, 0, 0)
EFFECT.Velocity = {120, 160}
EFFECT.VelocityRand = {-15, 40}
EFFECT.VelocityRandAngle = 12.5

EFFECT.ShellPresets = {
	["sniper"] = {"models/weapons/shell_rifle.mdl", math.pow(0.487 / 1.236636, 1 / 3), 90}, --1.236636 is shell diameter, then divide base diameter into that for 7.62x54mm
	["rifle"] = {"models/weapons/shell_rifle.mdl", math.pow(0.4709 / 1.236636, 1 / 3), 90}, --1.236636 is shell diameter, then divide base diameter into that for standard nato rifle
	["pistol"] = {"models/weapons/shell_pistol.mdl", math.pow(0.391 / 0.955581, 1 / 3), 90}, --0.955581 is shell diameter, then divide base diameter into that for 9mm luger
	["smg"] = {"models/weapons/shell_pistol.mdl", math.pow(.476 / 0.955581, 1 / 3), 90}, --.45 acp
	["shotgun"] = {"models/weapons/shell_buck.mdl", 1, 90, 3} --overrides smoke offset with 3
}

EFFECT.SoundFiles = {Sound(")player/pl_shell1.wav"), Sound(")player/pl_shell2.wav"), Sound(")player/pl_shell3.wav")}
EFFECT.SoundFilesSG = {Sound(")weapons/fx/tink/shotgun_shell1.wav"), Sound(")weapons/fx/tink/shotgun_shell2.wav"), Sound(")weapons/fx/tink/shotgun_shell3.wav")}
EFFECT.SoundLevel = {45, 55}
EFFECT.SoundPitch = {80, 120}
EFFECT.SoundVolume = {0.85, 0.95}
EFFECT.LifeTime = 0
EFFECT.FadeTime = 0.5
EFFECT.SmokeRate = 60 --Particles per second, multiplied by velocity down to 10%
EFFECT.SmokeTime = {5, 7}
EFFECT.SmokeThickness = {2, 2} --Particles on each puff
EFFECT.SmokeOffsetLength = 3 --just default, gets replaced later
local cv_eject
local cv_life

function EFFECT:Init(data)
	if (LocalPlayer():ShouldDrawLocalPlayer()) then return end

	if not cv_eject then
		cv_eject = GetConVar("cl_tfa_fx_ejectionsmoke")
	end

	if not cv_life then
		cv_life = GetConVar("cl_tfa_fx_ejectionlife")
	end

	if cv_life then
		self.LifeTime = cv_life:GetFloat()
	end

	self.StartTime = CurTime()
	self.Emitter = ParticleEmitter(self:GetPos())
	self.SmokeDelta = 0

	if cv_eject:GetBool() then
		self.SmokeDeath = self.StartTime + math.Rand(self.SmokeTime[1], self.SmokeTime[2])
	else
		self.SmokeDeath = -1
	end

	self.Position = bvec
	self.WeaponEnt = data:GetEntity()
	if not IsValid(self.WeaponEnt) then return end
	self.WeaponEntOG = self.WeaponEnt
	if self.WeaponEntOG.LuaShellEffect and self.WeaponEntOG.LuaShellEffect == "" then return end
	self.Attachment = data:GetAttachment()
	self.Dir = data:GetNormal()
	local owent = self.WeaponEnt:GetOwner()

	if self.LifeTime <= 0 or not IsValid(owent) then
		self.StartTime = -1000
		self.SmokeDeath = -1000

		return
	end


	if owent:IsPlayer() then
		if owent ~= GetViewEntity() or owent:ShouldDrawLocalPlayer() then
			self.WeaponEnt = owent:GetActiveWeapon()
			if not IsValid(self.WeaponEnt) then return end
		else
			self.WeaponEnt = owent:GetViewModel()
			local theirweapon = owent:GetActiveWeapon()

			if IsValid(theirweapon) and theirweapon.ViewModelFlip or theirweapon.ViewModelFlipped then
				self.Flipped = true
			end

			if not IsValid(self.WeaponEnt) then return end
		end
	end

	self:SetParent(self.WeaponEnt)
	local angpos = self:GetAttachmentInfo()

	local model, scale, yaw, len = self:FindModel(self.WeaponEntOG)
	model = self.WeaponEntOG:GetStat("ShellModel") or self.WeaponEntOG:GetStat("LuaShellModel") or model
	scale = self.WeaponEntOG:GetStat("ShellScale") or self.WeaponEntOG:GetStat("LuaShellScale") or scale
	ang = self.WeaponEntOG:GetStat("ShellAng") or self.WeaponEntOG:GetStat("LuaShellAng") or Angle()
    local shellPos = self.WeaponEntOG:GetStat("ShellPos") or self.WeaponEntOG:GetStat("LuaShellPos") or Vector()
    local ejectPos = angpos.Pos
    ejectPos = ejectPos + self.WeaponEnt:GetForward() * shellPos.x
    ejectPos = ejectPos + self.WeaponEnt:GetRight() * shellPos.y
    ejectPos = ejectPos + self.WeaponEnt:GetUp() * shellPos.z

	if model:lower():find("shotgun") then
		self.Shotgun = true
	end

	self:SetModel(model)
	self:SetModelScale(scale, 0)
	self:SetPos(ejectPos)
	local mdlang = angpos.Ang * 1
	local owang = IsValid(owent) and owent:EyeAngles() or mdlang
	owang:RotateAroundAxis(mdlang:Right(), ang.p)
	owang:RotateAroundAxis(mdlang:Up(), ang.y)
	owang:RotateAroundAxis(mdlang:Forward(), ang.r)
	owang:RotateAroundAxis(owang:Forward(), math.Rand(-20, -10))
	owang:RotateAroundAxis(owang:Right(), math.Rand(-6, 6))
	self:SetAngles(owang)
	self:SetSequence(math.random(1, 2))
	self.ShellSpeed = math.Rand(1.2, 1.4)
	self:ManipulateBoneAngles(0, Angle(0, math.Rand(0, 30), math.Rand(15, -15)))

	local velocity = angpos.Ang:Forward() * math.Rand(self.Velocity[1], self.Velocity[2]) + owang:Forward() * math.Rand(self.VelocityRand[1], self.VelocityRand[2])

	if IsValid(owent) then
		velocity = velocity + owent:GetVelocity()
	end

	self.NextNode = CurTime()
	self.TrailNodes = {}
	self.setup = true
	self.RandomOffset = math.Rand(2, 4)
	self.RandomAlpha = math.Rand(.5, 1)
end

function EFFECT:GetAttachmentInfo()
	if IsValid(self.WeaponEntOG) and self.WeaponEntOG.ShellAttachment then
		self.Attachment = self.WeaponEnt:LookupAttachment(self.WeaponEntOG.ShellAttachment)

		if not self.Attachment or self.Attachment <= 0 then
			self.Attachment = 2
		end

		if self.WeaponEntOG.Akimbo then
			self.Attachment = 3 + (game.SinglePlayer() and self.WeaponEntOG:GetNW2Int("AnimCycle", 1) or self.WeaponEntOG.AnimCycle)
		end

		if self.WeaponEntOG.ShellAttachmentRaw then
			self.Attachment = self.WeaponEntOG.ShellAttachmentRaw
		end
	end

	local angpos = self.WeaponEnt:GetAttachment(self.Attachment)

	if not angpos or not angpos.Pos then
		angpos = {
			Pos = bvec,
			Ang = uAng
		}
	end

	return angpos
end
function EFFECT:FindModel(wep)
	if not IsValid(wep) then return unpack(self.ShellPresets["rifle"]) end
	local ammotype = (wep.Primary.Ammo or wep:GetPrimaryAmmoType()):lower()
	local guntype = (wep.Type or wep:GetHoldType()):lower()

	if guntype:find("sniper") or ammotype:find("sniper") or guntype:find("dmr") then
		return unpack(self.ShellPresets["sniper"])
	elseif guntype:find("rifle") or ammotype:find("rifle") then
		return unpack(self.ShellPresets["rifle"])
	elseif ammotype:find("pist") or guntype:find("pist") then
		return unpack(self.ShellPresets["pistol"])
	elseif ammotype:find("smg") or guntype:find("smg") then
		return unpack(self.ShellPresets["smg"])
	elseif ammotype:find("buckshot") or ammotype:find("shotgun") or guntype:find("shot") then
		return unpack(self.ShellPresets["shotgun"])
	end

	return unpack(self.ShellPresets["rifle"])
end

function EFFECT:Think()
	if (LocalPlayer():ShouldDrawLocalPlayer()) then return end

	local cycle = (CurTime() * self.ShellSpeed) - (self.StartTime * self.ShellSpeed)
	self:SetCycle(cycle)

	if (cycle >= 1) then
		self.TrailNodes = nil
		if self.Emitter then
			self.Emitter:Finish()
		end

		return false
	else
		return true
	end
end

-- WorldToLocal( Vector position, Angle angle, Vector newSystemOrigin, Angle newSystemAngles )
function EFFECT:GetTrailPos(forcePos)
	local info = self:GetAttachmentInfo()
	local pos, ang = self:GetBonePosition(0)
	local pos = pos + ang:Up() * -1
	return WorldToLocal(forcePos or pos, ang, EyePos(), EyeAngles()) + VectorRand() * math.Rand(.1, 1)
end

local NextNodeTime = .025
local SmokeTraceMaterial = Material( "particles/tfa_ins2/ins_smoke_beam" )
local size = 2
local maxTrail = 8
local trailFadeIn = 5
local trailFadeOut = maxTrail

function EFFECT:Render()
	if (LocalPlayer():ShouldDrawLocalPlayer()) then return end
	
	if not self.setup then return end
	self:SetColor(ColorAlpha(color_white, 10))
	self:SetupBones()
	self:DrawModel()
	
	local count = #self.TrailNodes

	for index = math.max(1, count - maxTrail), count do
		self.TrailNodes[index] = self.TrailNodes[index] + Vector(FrameTime(), FrameTime(), FrameTime() * math.random(5, 10))
	end
	 
	if (self.NextNode < CurTime()) then
		self.TrailNodes[count + 1] = self:GetTrailPos()

		self.NextNode = CurTime() + NextNodeTime
	end
	count = #self.TrailNodes
	self.TrailNodes[count] = self:GetTrailPos()

	local cycle = ((CurTime() * self.ShellSpeed) - (self.StartTime * self.ShellSpeed)) * 2
	render.SetMaterial( SmokeTraceMaterial )
	render.StartBeam(math.min(maxTrail, count))
		local initTrail = false
		for index = math.max(1, count - maxTrail), count do
			position = self.TrailNodes[index]

			if (position) then
				local lerpFadeIn, lerpFadeOut = Lerp((count - index)/trailFadeIn, 0, 1), Lerp((count - index)/trailFadeOut, 1, 0)
				local decay = self.RandomAlpha * math.Clamp(math.min(lerpFadeIn, lerpFadeOut), 0, 1)

				render.AddBeam(
					LocalToWorld(position, Angle(), EyePos(), EyeAngles()),
					lerpFadeIn * self.RandomOffset,
					self.RandomOffset + index * .5 + CurTime() * 3 * -self.RandomAlpha,
					ColorAlpha(Color(188, 188, 188)
					, !initTrail and 0 or 255 * decay)
				)

				initTrail = true
			end
		end
	render.EndBeam()
end