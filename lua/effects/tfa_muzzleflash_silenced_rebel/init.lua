local AddVel = Vector()
local ang

function EFFECT:Init(data)
	self.WeaponEnt = data:GetEntity()
	if not IsValid(self.WeaponEnt) then return end
	self.Attachment = data:GetAttachment()
	self.Position = self:GetTracerShootPos(data:GetOrigin(), self.WeaponEnt, self.Attachment)

	local scale = self.WeaponEnt:GetStat("MuzzleFlashScale") or .8
	if IsValid(self.WeaponEnt:GetOwner()) then
		if self.WeaponEnt:GetOwner() == LocalPlayer() then
			if self.WeaponEnt:GetOwner():ShouldDrawLocalPlayer() then
				ang = self.WeaponEnt:GetOwner():EyeAngles()
				ang:Normalize()
				--ang.p = math.max(math.min(ang.p,55),-55)
				self.Forward = ang:Forward()
			else
				self.WeaponEnt = self.WeaponEnt:GetOwner():GetViewModel()
			end
			--ang.p = math.max(math.min(ang.p,55),-55)
		else
			ang = self.WeaponEnt:GetOwner():EyeAngles()
			ang:Normalize()
			self.Forward = ang:Forward()
		end
	end

	self.Forward = self.Forward or data:GetNormal()
	self.Angle = self.Forward:Angle()
	self.Right = self.Angle:Right()
	self.vOffset = self.Position
	dir = self.Forward

	local owner = self.WeaponEnt:GetOwner()
	if not IsValid(ownerent) then
		ownerent = LocalPlayer()
	end
	AddVel = ownerent:GetVelocity()

	self.vOffset = self.Position
	dir = self.Forward
	AddVel = AddVel * 0.05
	local dot = dir:GetNormalized():Dot(GetViewEntity():EyeAngles():Forward())
	local dotang = math.deg(math.acos(math.abs(dot)))
	local halofac = math.abs(dot)
	local epos = ownerent:GetShootPos()

	local dlight = DynamicLight(ownerent:EntIndex())

	local muzzleTime = .07

	if (dlight) then
		dlight.pos = epos + ownerent:EyeAngles():Forward() * self.vOffset:Distance(epos) + 1.05 * ownerent:GetVelocity() * FrameTime()--self.vOffset - ownerent:EyeAngles():Right() * 5 + 1.05 * ownerent:GetVelocity() * FrameTime()
		dlight.r = 255
		dlight.g = 192
		dlight.b = 64
		dlight.brightness = .5
		dlight.Decay = 2048
		dlight.Size = 128
		dlight.DieTime = CurTime() + muzzleTime
	end

	self.FreeEmitter = ParticleEmitter(self.vOffset)
	self.Emitter = ParticleEmitter(Vector())
	self.Emitter:SetNoDraw(true)
	self.MuzzleTime = CurTime() + muzzleTime * 2

	local muzInfo = {
		{
			v = math.Rand(222, 111)*Vector(1, 0, 0),
			s = math.random(1,2),
			e = math.random(5,6),
		},
		{
			v = math.Rand(351, 112)*Vector(1, 0, 0),
			s = math.random(12,6),
			e = math.random(2,1),
			l = math.random(11,22),
			a = 33
		},
		{
			m = "effects/scotchmuzzleflash4",
			v = math.Rand(111, 222)*Vector(1, 0, 0),
			s = math.random(8,4),
			e = math.random(1,4),
			a = 244
		},
		{
			m = "effects/scotchmuzzleflash1",
			v = math.Rand(222, 444)*Vector(1, 0, 0),
			s = math.random(5,11),
			e = math.random(11,22),
		},
		{
			m = "particle/Particle_Glow_04_Additive",
			v = math.Rand(111, 222)*Vector(1, 0, 0),
			s = math.random(11,3),
			e = math.random(2,1),
			a = 15,
			c = Color(255, 155, 0)
		},
	}

	for k, v in pairs(muzInfo) do
		local p = self.Emitter:Add(v.m or TFACW_MUZZLEMATS[math.random(1, 8)], Vector(3, 0, 0))
		p:SetVelocity(v.v * scale)
		p:SetDieTime(math.Rand(muzzleTime * .5, muzzleTime))
		p:SetStartAlpha(v.a or 255)
		p:SetEndAlpha(0)
		p:SetStartSize(v.s * scale)
		p:SetEndSize(v.e * scale)
		if (v.l) then
			p:SetStartLength(v.l * scale)
			p:SetEndLength(v.l * .5 * scale)
		end
		p:SetRoll(math.Rand(180,480))
		local c = v.c or Color(255, 255, 222)
		p:SetColor(c.r, c.g, c.b)
		p:SetRollDelta(math.Rand(-3,3))
	end
	
	local particle = self.Emitter:Add("particle/smokesprites_000"..math.random(4,9), Vector(0, 0, 0))

	particle:SetVelocity(VectorRand() * 10 + Vector(1, 0, 0) * math.Rand(111, 222) * scale)
	particle:SetLifeTime(0)
	particle:SetDieTime(muzzleTime * 2)
	particle:SetStartAlpha(math.Rand(55, 111) )
	particle:SetEndAlpha(0)
	particle:SetStartSize(math.Rand(1, 2) * scale)
	particle:SetEndSize(math.Rand(5, 11) * scale)
	particle:SetRoll(math.rad(math.Rand(0, 360)))
	particle:SetRollDelta(math.Rand(-0.8, 0.8))
	particle:SetLighting(true)
	particle:SetAirResistance(10)
	particle:SetGravity(Vector(0, 0, 60))
	particle:SetColor(255, 255, 255)

	local particle = self.Emitter:Add("effects/yellowflare", Vector())
	if (particle) then
		particle:SetVelocity(VectorRand() * 1 + Vector(1, 0, 0) * 8 )
		particle:SetLifeTime(0)
		particle:SetDieTime(math.Rand(0.2, 0.4))
		particle:SetStartAlpha(255)
		particle:SetEndAlpha(0)
		particle:SetStartSize(0.5)
		particle:SetEndSize(1.0)
		particle:SetRoll(math.rad(math.Rand(0, 360)))
		particle:SetGravity(vector_origin)
		particle:SetAirResistance(20)
		particle:SetStartLength(0.1)
		particle:SetEndLength(0.1)
		particle:SetColor(255, math.random(192,225), math.random(140,192))
		particle:SetVelocityScale(true)

		particle:SetThinkFunction(function(pa)
			pa.ranvel = pa.ranvel or VectorRand() * 15
			pa.ranvel.x = math.Approach(pa.ranvel.x, math.Rand(-4, 4), 0.5)
			pa.ranvel.y = math.Approach(pa.ranvel.y, math.Rand(-4, 4), 0.5)
			pa.ranvel.z = math.Approach(pa.ranvel.z, math.Rand(-4, 4), 0.5)
			pa:SetVelocity(pa:GetVelocity() + pa.ranvel * 0.3)
			pa:SetNextThink(CurTime() + 0.01)
		end)

		particle:SetNextThink(CurTime() + 0.01)
	end

	for i = 0, 4 do
		local particle = self.FreeEmitter:Add("particle/smokesprites_000"..math.random(4,9), self.vOffset + dir * 4 * i)

		particle:SetVelocity(VectorRand() * 10 + dir * math.Rand(33, 11) * i + 1.05 * AddVel )
		particle:SetLifeTime(0)
		particle:SetDieTime(muzzleTime * 2.5 * Lerp(i/4, .5, 1))
		particle:SetStartAlpha(math.Rand(44, 33) * i)
		particle:SetEndAlpha(0)
		particle:SetStartSize(math.Rand(5, 16))
		particle:SetEndSize(math.Rand(33, 22) * Lerp(i/4, 1, 1.5))
		particle:SetRoll(math.rad(math.Rand(0, 360)))
		particle:SetRollDelta(math.Rand(-0.8, 0.8))
		particle:SetLighting(true)
		particle:SetAirResistance(10)
		particle:SetGravity(Vector(0, 0, 60))
		particle:SetColor(255, 255, 255)
	end
end

function EFFECT:Think()
	if (self.MuzzleTime < CurTime()) then 
		self.Emitter:Finish()

		return false
	end

	return true
end

function EFFECT:Render()
	if (IsValid(self.WeaponEnt) and IsValid(self.Emitter)) then
		local at = self.WeaponEnt:GetAttachment(self.Attachment)
		
		if (at) then
			self.Emitter:DrawAt(at.Pos, at.Ang)
		end
	end
end