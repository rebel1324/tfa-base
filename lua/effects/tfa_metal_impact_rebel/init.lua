
local gravity_cv = GetConVar("sv_gravity")
EFFECT.VelocityRandom = 0.5
EFFECT.VelocityMin = 222
EFFECT.VelocityMax = 111
EFFECT.ParticleCountMin = 4
EFFECT.ParticleCountMax = 7
EFFECT.ParticleLife = 1.3


TFACW_METALIMPACT = {}
for i = 0, 1 do
	TFACW_METALIMPACT[i] = Material("effects/spark0" .. i)
	TFACW_METALIMPACT[i + 2] = Material("effects/spark1" .. i)
end

local metalImpactTime = .12
function EFFECT:Init(data)
	self.Normal = data:GetNormal()
	self.LifeTime = CurTime() + metalImpactTime
	self.MaxSize = math.Rand(32, 64)
	self.RandomRotation = math.Rand(0, 360)
	self.Grav = Vector(0, 0, -gravity_cv:GetFloat())

	local pos = self:GetPos()
	local normal = self.Normal
	self.Emitter = ParticleEmitter(pos)

	local particle = self.Emitter:Add("particle/Particle_Glow_04_Additive", pos)
	particle:SetVelocity(normal * 333)
	particle:SetLifeTime(0)
	particle:SetDieTime(metalImpactTime * .5)
	particle:SetStartAlpha(math.Rand(155, 22) )
	particle:SetEndAlpha(0)
	particle:SetStartSize(math.Rand(22, 44) )
	particle:SetEndSize(math.Rand(44, 66) )
	particle:SetRoll(math.rad(math.Rand(0, 360)))
	particle:SetRollDelta(math.Rand(-0.8, 0.8))
	particle:SetAirResistance(10)
	particle:SetGravity(Vector(0, 0, 60))
	particle:SetColor(88, 39, 49)

	local partcount = math.random(self.ParticleCountMin, self.ParticleCountMax)

	--Sparks
	for i = 1, partcount do
		local part = self.Emitter:Add("effects/yellowflare", pos)
		part:SetVelocity(Lerp(self.VelocityRandom, normal, VectorRand()) * math.Rand(self.VelocityMin, self.VelocityMax))
		part:SetDieTime(math.Rand(0.25, 1))
		part:SetStartAlpha(255)
		part:SetStartSize(math.Rand(1, 4))
		part:SetEndSize(0)
		part:SetRoll(0)
		part:SetGravity(self.Grav)
		part:SetCollide(true)
		part:SetBounce(0.55)
		part:SetAirResistance(10.5)
		part:SetStartLength(0.1)
		part:SetEndLength(0)
		part:SetVelocityScale(true)
		part:SetCollide(true)
	end

	self.Emitter:Finish()
end

function EFFECT:Think()
	if (self.LifeTime > CurTime()) then
		return true
	end

	return false
end

-- lmao
function EFFECT:Render()
	local cycle = (self.LifeTime - CurTime())/.1
	local size = Lerp(cycle, self.MaxSize, self.MaxSize/2)
	local targetPos = self:GetPos() + self.Normal * size*.1
	local eyePos = EyePos()
	local dir =  EyePos() - targetPos
	dir:Normalize()
	
	render.SetMaterial(TFACW_METALIMPACT[math.min(3, math.floor(Lerp(cycle, 4, 0)))])
	render.DrawQuadEasy(targetPos, dir, size, size, color_white, self.RandomRotation)

	return false
end
/*
effects.Register({
	Init = function() return false end,
	Think = function() return false end,
	Render = function() return false end
}, "Impact")
*/