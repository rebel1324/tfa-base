
local AddVel = Vector()
local ang


local META = FindMetaTable("CLuaEmitter")
if not META then return end
function META:DrawAt(pos, ang, fov)
	local pos, ang = WorldToLocal(EyePos(), EyeAngles(), pos, ang)
	
	cam.Start3D(pos, ang, fov)
		self:Draw()
	cam.End3D()
end

function EFFECT:Init(data)
	self.WeaponEnt = data:GetEntity()
	if not IsValid(self.WeaponEnt) then return end
	self.Attachment = data:GetAttachment()
	self.Position = self:GetTracerShootPos(data:GetOrigin(), self.WeaponEnt, self.Attachment)

	local scale = self.WeaponEnt:GetStat("MuzzleFlashScale") or .25
	self.MuzzleAdjust = self.WeaponEnt:GetStat("MuzzleDirection") or Angle()

	if IsValid(self.WeaponEnt:GetOwner()) then
		if self.WeaponEnt:GetOwner() == LocalPlayer() then
			if self.WeaponEnt:GetOwner():ShouldDrawLocalPlayer() then
				ang = self.WeaponEnt:GetOwner():EyeAngles()
				ang:Normalize()
				--ang.p = math.max(math.min(ang.p,55),-55)
				self.Forward = ang:Forward()
			else
				self.WeaponEnt = self.WeaponEnt:GetOwner():GetViewModel()
			end
			--ang.p = math.max(math.min(ang.p,55),-55)
		else
			ang = self.WeaponEnt:GetOwner():EyeAngles()
			ang:Normalize()
			self.Forward = ang:Forward()
		end
	end

	self.Forward = self.Forward or data:GetNormal()
	self.Angle = self.Forward:Angle()
	self.Right = self.Angle:Right()
	self.vOffset = self.Position
	dir = self.Forward

	local owner = self.WeaponEnt:GetOwner()
	if not IsValid(ownerent) then
		ownerent = LocalPlayer()
	end
	AddVel = ownerent:GetVelocity()

	self.vOffset = self.Position
	dir = self.Forward
	AddVel = AddVel * 0.05
	local dot = dir:GetNormalized():Dot(GetViewEntity():EyeAngles():Forward())
	local dotang = math.deg(math.acos(math.abs(dot)))
	local halofac = math.abs(dot)
	local epos = ownerent:GetShootPos()

	local dlight = DynamicLight(ownerent:EntIndex())

	if (dlight) then
		dlight.pos = epos + ownerent:EyeAngles():Forward() * self.vOffset:Distance(epos) + 1.05 * ownerent:GetVelocity() * FrameTime()--self.vOffset - ownerent:EyeAngles():Right() * 5 + 1.05 * ownerent:GetVelocity() * FrameTime()
		dlight.r = 255
		dlight.g = 192
		dlight.b = 64
		dlight.brightness = 4.5
		dlight.Decay = 500
		dlight.Size = 128
		dlight.DieTime = CurTime() + 0.2
	end

	local muzzleTime = .1
	self.FreeEmitter = ParticleEmitter(self.vOffset)
	self.Emitter = ParticleEmitter(Vector())
	self.Emitter:SetNoDraw(true)
	self.MuzzleTime = CurTime() + muzzleTime

	local muzInfo = {
		{
			v = math.Rand(222, 333)*Vector(1, 0, 0),
			s = math.random(11,22),
			e = 11,
		},
		{
			v = math.Rand(111, 222)*Vector(1, 0, 0),
			s = math.random(6,11),
			e = math.random(4,8),
		},
		{
			v = 666*Vector(1, 0, 0),
			s = 44,
			e = 33,
		},
		{
			v = math.Rand(333, 444)*Vector(1, 0, 0),
			s = math.random(11,22),
			e = math.random(5,11),
		},
		{
			v = math.Rand(1, 2)*Vector(1, 0, 0),
			s = math.random(44,33),
			e = math.random(44,33),
			l = math.random(77,88),
		},
		{
			m = "effects/scotchmuzzleflash1",
			v = Vector(155, 0, 0),
			s = 33,
			e = 5,
		},
		{
			m = "effects/scotchmuzzleflash4",
			v = Vector(55, 0, 0),
			s = 22,
			e = 5,
		},
		{
			m = "particle/Particle_Glow_04_Additive",
			v = math.Rand(322, 333)*Vector(1, 0, 0),
			s = math.random(33,44),
			e = math.random(4,8),
			a = 155,
			c = Color(255, 155, 0)
		},
	}

	for k, v in pairs(muzInfo) do
		local p = self.Emitter:Add(v.m or TFACW_MUZZLEMATS[math.random(1, 8)], Vector(3, 0, 0))
		p:SetVelocity(v.v * scale)
		p:SetDieTime(math.Rand(muzzleTime * .5, muzzleTime))
		p:SetStartAlpha(v.a or 255)
		p:SetEndAlpha(0)
		p:SetStartSize(v.s * scale)
		p:SetEndSize(v.e * scale)
		if (v.l) then
			p:SetStartLength(v.l * scale)
			p:SetEndLength(v.l * .5 * scale)
		end
		p:SetRoll(math.Rand(180,480))
		local c = v.c or Color(255, 255, 222)
		p:SetColor(c.r, c.g, c.b)
		p:SetRollDelta(math.Rand(-3,3))
	end
	
	local particle = self.Emitter:Add("effects/yellowflare", Vector())
	if (particle) then
		particle:SetVelocity(VectorRand() * 15 + Vector(1, 0, 0) * 188 * scale )
		particle:SetLifeTime(0)
		particle:SetDieTime(math.Rand(0.2, 0.4))
		particle:SetStartAlpha(255)
		particle:SetEndAlpha(0)
		particle:SetStartSize(0.5)
		particle:SetEndSize(1)
		particle:SetRoll(math.rad(math.Rand(0, 360)))
		particle:SetGravity(vector_origin)
		particle:SetAirResistance(20)
		particle:SetColor(255, math.random(192,225), math.random(140,192))
		particle:SetVelocityScale(true)

		particle:SetThinkFunction(function(pa)
			pa.ranvel = pa.ranvel or VectorRand() * 15
			pa.ranvel.x = math.Approach(pa.ranvel.x, math.Rand(-4, 4), 0.5)
			pa.ranvel.y = math.Approach(pa.ranvel.y, math.Rand(-4, 4), 0.5)
			pa.ranvel.z = math.Approach(pa.ranvel.z, math.Rand(-4, 4), 0.5)
			pa:SetVelocity(pa:GetVelocity() + pa.ranvel * 0.3)
			pa:SetNextThink(CurTime() + 0.01)
		end)

		particle:SetNextThink(CurTime() + 0.01)
	end

	local particle = self.Emitter:Add("particle/smokesprites_000"..math.random(4,9), Vector(0, 0, 0))
	particle:SetVelocity(VectorRand() * 10 + Vector(1, 0, 0) * math.Rand(111, 222) * scale)
	particle:SetLifeTime(0)
	particle:SetDieTime(muzzleTime * 2)
	particle:SetStartAlpha(math.Rand(55, 111) )
	particle:SetEndAlpha(0)
	particle:SetStartSize(math.Rand(1, 2) * scale)
	particle:SetEndSize(math.Rand(5, 11) * scale)
	particle:SetRoll(math.rad(math.Rand(0, 360)))
	particle:SetRollDelta(math.Rand(-0.8, 0.8))
	particle:SetLighting(true)
	particle:SetAirResistance(10)
	particle:SetGravity(Vector(0, 0, 60))
	particle:SetColor(255, 255, 255)

	for i = 0, 4 do
		local particle = self.FreeEmitter:Add("particle/smokesprites_000"..math.random(4,9), self.vOffset + dir * 4 * i)

		particle:SetVelocity(VectorRand() * 10 + dir * math.Rand(33, 11) * i + 1.05 * AddVel )
		particle:SetLifeTime(0)
		particle:SetDieTime(muzzleTime * 2.5 * Lerp(i/4, .5, 1))
		particle:SetStartAlpha(math.Rand(44, 22) * i)
		particle:SetEndAlpha(0)
		particle:SetStartSize(math.Rand(5, 16))
		particle:SetEndSize(math.Rand(15, 27) * Lerp(i/4, 1, 1.5))
		particle:SetRoll(math.rad(math.Rand(0, 360)))
		particle:SetRollDelta(math.Rand(-0.8, 0.8))
		particle:SetLighting(true)
		particle:SetAirResistance(10)
		particle:SetGravity(Vector(0, 0, 60))
		particle:SetColor(255, 255, 255)
	end
end

function EFFECT:Think()
	if (self.MuzzleTime < CurTime()) then 
		self.Emitter:Finish()

		return false
	end

	return true
end

function EFFECT:Render()
	if (IsValid(self.WeaponEnt) and IsValid(self.Emitter)) then
		local at = self.WeaponEnt:GetAttachment(self.Attachment)
		local oang = at.Ang
		at.Ang:RotateAroundAxis(oang:Right(), self.MuzzleAdjust.p)
		at.Ang:RotateAroundAxis(oang:Up(), self.MuzzleAdjust.y)
		at.Ang:RotateAroundAxis(oang:Forward(), self.MuzzleAdjust.r)
		
		self.Emitter:DrawAt(at.Pos, at.Ang)
	end
end