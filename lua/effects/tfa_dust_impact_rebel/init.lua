local sparks = {}
for i = 1, 9 do
	sparks[i] = "particle/smokesprites_000"..i
end

function EFFECT:Init(data)
	local origin = data:GetOrigin()
	local normal = data:GetNormal()
	self.emitter = ParticleEmitter(Vector(0, 0, 0))
	self.lifeTime = CurTime() + 1
	local col = render.GetSurfaceColor(origin + normal, origin - normal) * 255
	for i = 1, 3 do
		col[i] = math.max(255, col[i])
	end
	local dustAlpha = OVERALL_IMPACT_ALPHA

	for i = 1, 3 do
		local daym = Lerp(i/3, 1, .5)
		local p = self.emitter:Add(sparks[math.random(1,9)], origin + normal)
		p:SetVelocity(222*normal*i)
		p:SetDieTime(.12)
		p:SetStartAlpha(dustAlpha*daym)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(22,24)*daym)
		p:SetEndSize(math.random(22,24)*daym)
		p:SetRoll(math.Rand(180,480))
		p:SetRollDelta(math.Rand(-3,3))
		p:SetAirResistance(300)
		p:SetColor(col)
		p:SetGravity( Vector( 0, 0, 100 )*math.Rand( .2, 1 ) )
	end
	
	local p = self.emitter:Add("effects/yellowflare", origin + normal*4)
	p:SetVelocity(11*normal)
	p:SetDieTime(.1)
	p:SetStartAlpha(155)
	p:SetEndAlpha(0)
	p:SetEndSize(math.random(16,11))
	p:SetStartSize(0)
	p:SetRoll(math.Rand(180,480))
	p:SetRollDelta(math.Rand(-3,3))
	p:SetColor(255,230,200)
	p:SetGravity( Vector( 0, 0, 100 )*math.Rand( .2, 1 ) )

	for i = 1, 2 do
		local p = self.emitter:Add(sparks[math.random(1, 9)], origin + normal*2)
		local dir = (normal + VectorRand()*5):Angle()
		p:SetVelocity(10*dir:Forward())
		p:SetDieTime(.1)
		p:SetStartAlpha(dustAlpha * 2)
		p:SetEndAlpha(0)
		p:SetStartSize(math.random(11,33))
		p:SetEndSize(4)
		p:SetStartLength(0)
		p:SetEndLength(math.Rand(1, 44))
		p:SetAirResistance(1000)
		p:SetColor(col)
	end

	local p = self.emitter:Add(sparks[math.random(1, 9)], origin - normal*1)
	local dir = (normal + VectorRand()*.5):Angle()
	p:SetVelocity(10*dir:Forward())
	p:SetDieTime(.1)
	p:SetStartAlpha(dustAlpha * 2)
	p:SetEndAlpha(0)
	p:SetStartSize(math.random(11,33))
	p:SetEndSize(4)
	p:SetStartLength(0)
	p:SetEndLength(math.Rand(44, 88))
	p:SetAirResistance(1000)
	p:SetColor(col)

	self.emitter:Finish()
end
function EFFECT:Render()
	return false
end

function EFFECT:Think()	
	return false
end
